package proj.api.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import proj.api.json.TransactionRequest;
import proj.api.json.TransactionResponse;
import proj.console.dao.UserEntity;
import proj.security.CustomUserDetails;
import proj.service.AuthService;
import proj.service.TransactionService;

import javax.validation.Valid;
import java.math.BigDecimal;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TransactionController {
    private final TransactionService transactionService;
    private final AuthService authService;

    @PostMapping("/trans")
    public @ResponseBody ResponseEntity<TransactionResponse> transaction(@RequestBody @Valid TransactionRequest req) {

        transactionService.insertTransaction(
                currentUser().getId(),
                req.getAccountIdFrom(),
                req.getAccountIdTo(),
                new BigDecimal(req.getAmount()),
                req.getCatId());

        return ok(new TransactionResponse("Транзакция прошла успешно!"));
    }

    private UserEntity currentUser() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return authService.getUserById(user.getId());
    }
}