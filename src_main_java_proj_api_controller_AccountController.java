package proj.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import proj.api.converter.AccountToResponseConverter;
import proj.console.dao.Account;
import proj.api.json.*;
import proj.console.dao.UserEntity;
import proj.security.CustomUserDetails;
import proj.service.AccountService;
import proj.service.AuthService;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AccountController {
    private final AccountService accountService;
    private final AuthService authService;
    private final AccountToResponseConverter converter;

    @GetMapping("/account/select")
    public @ResponseBody ResponseEntity<AccountsListResponse> selectAcc() {
        AccountsListResponse listResponse = new AccountsListResponse(
                accountService.selectAccounts(currentUser().getId())
                .stream()
                .map(converter::convert)
                .collect(Collectors.toList()));

        if(listResponse == null) {
            status(HttpStatus.NOT_FOUND);
        }
        return ok(listResponse);
    }

    @PostMapping("/account/new")
    public @ResponseBody ResponseEntity<AccountResponse> newAcc(@RequestBody @Valid AccountNewRequest request) {
        Account save = accountService.newAccount(currentUser().getId(),
                request.getAccountName(),
                new BigDecimal(request.getAccountBalance()));

        if (save == null) {
            return status(HttpStatus.BAD_REQUEST).build();
        }
        return ok(new AccountResponse("Новый счет создан!"));
    }

    @PutMapping("/account/up")
    public @ResponseBody ResponseEntity<AccountResponse> updateAcc(@RequestBody @Valid AccountUpRequest request) {
        Account newAccount = accountService.upAccount(currentUser().getId(),
                request.getAccountId(),
                request.getNameAcc());

        if(newAccount == null) {
            status(HttpStatus.BAD_REQUEST).build();
        }
        return ok(new AccountResponse("Счет переименован!"));
    }

    @DeleteMapping("/account/del")
    public @ResponseBody ResponseEntity<AccountResponse> deleteAcc(@RequestBody @Valid AccountDelRequest request) {
        if (!accountService.delAccount(currentUser().getId(),
                request.getAccountId())) {
            status(HttpStatus.BAD_REQUEST).build();
        }
        return ok(new AccountResponse("Счет удален!"));
    }

    private UserEntity currentUser() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return authService.getUserById(user.getId());
    }
}
